module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    less: {
      options: {
          sourceMap:true,
          //outputSourceFiles: true
      },
      build: {
        files: {
          '../../planer_2_back_C_1/planer_back/static/css/style_n.css': ['less/style.less'],
        }
      }
    },

    watch: {
      stylesheets: {
        files: ['**/*.less'],
        tasks: ['less']
      }
    }

  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['less', 'watch']);

};
